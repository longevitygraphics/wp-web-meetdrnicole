<?php
/**
 * The header for our theme
 *
 */

?>

<?php do_action('document_start'); ?>

<!doctype html>
<html <?php language_attributes(); ?> <?php do_action('html_class'); ?>>
<head>
  <!--[if lt IE 9]>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
  <![endif]-->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
  <?php do_action('wp_header'); ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

  <?php do_action('wp_body_start'); ?>

  <div id="page" class="site">
  	<header id="site-header">

      <?php do_action('wp_utility_bar'); ?>

      <div class="header-main p-0">
        <div class="bg-primary py-3 px-3 d-flex justify-content-between align-items-center flex-wrap position-relative">
          <div class="header-wrapper">
          	<div class="mobile-toggle"><i class="fa fa-bars" aria-hidden="true"></i></div>
            <?php if (is_front_page()): ?>
                <?php get_template_part("/templates/template-parts/header/home-nav"); ?>
            <?php else : ?>
                <?php get_template_part("/templates/template-parts/header/main-nav"); ?>
            <?php endif ?>
          </div>
          <div class="menu-ctas d-md-flex justify-content-between">
          		<a class="px-2 py-1 border border-dark" href="tel:<?php echo do_shortcode('[lg-phone-main]') ?>"><i class="fas fa-phone"></i><?php echo do_shortcode("[lg-phone-main]"); ?></a>
          		<a class="d-none d-xl-block px-2 py-1 border border-dark" href="#contact">Book Now</a>
          </div>
        </div>
        <div class="site-logo py-3  py-sm-5">
        	<div class="m-auto">
        	<?php  echo site_logo(); ?>
        	</div>
        </div>
      </div>

    </header><!-- #masthead -->

    <div id="site-content" role="main">
      <?php do_action('wp_content_top'); ?>
