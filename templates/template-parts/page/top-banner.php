<?php 
	$top_banner_image = get_field("top_banner_image");
	$second_banner_image = get_field("top_banner_image_two");
	$acivate_form = get_field("activate_form");
	$top_banner_text = get_field("top_banner_text");
 ?>

<div class="top-banner">

	<?php if( $top_banner_image ) :?>
		 <div class="top-banner-image">
		 	<img src="<?php echo $top_banner_image['url']; ?>" alt="<?php echo $top_banner_image['lt']; ?>" />
		 	
		</div>
		<?php if ($top_banner_text): ?>
			 <div class="top-banner-overlay">
			 	<div class="container">
				 	<div class="top-banner-text"><?php echo $top_banner_text; ?></div>
				</div>
			</div>
		 <?php endif ?>
		 </div>
	<?php endif; ?>
	
 </div>