<?php

	global $lg_tinymce_custom;
	
	$lg_tinymce_custom = array(
	    'title' => 'Custom',
	    'items' =>  array(
	    	array(
				'title' => 'title',
	            'selector' => 'selector',
	            'classes' => 'classes'
			),
			array(
				'title' => 'Underline Center',
	            'selector' => 'h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6',
	            'classes' => 'heading-underline-center'
			),
			array(
				'title' => 'Underline Left',
	            'selector' => 'h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6',
	            'classes' => 'heading-underline-left'
			),
			array(
				'title' => 'Underline Center White',
	            'selector' => 'h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6',
	            'classes' => 'heading-underline-center-white'
			)
	    )
	);

?>