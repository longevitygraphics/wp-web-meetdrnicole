const webpack = require('webpack');
const path = require('path');
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

const webUrl = 'drnicoleclement.test';

module.exports = function(env) {
    return {
        devtool: 'source-map',
        entry: {
            main: "./webpack.js"
        },
        output: {
            path: path.join(__dirname, "assets/dist/js"),
            filename: "[name].js"
        },
        watchOptions: {
            ignored: ['node_modules', path.join(__dirname, "assets/dist")]
        },
        module: {
            rules: [
                {test: /\.html$/, loader: 'raw-loader', exclude: /node_modules/},
                {
                    test: /\.css$/,
                    use: [
                      "style-loader",
                      "css-loader"
                    ]
                },
                {
                    test: /\.scss$/,
                    use: [{
                        loader: MiniCssExtractPlugin.loader,
                    }, {
                        loader: "css-loader", options: {
                            sourceMap: true
                        }
                    },{
                        loader: 'postcss-loader', // Run post css actions
                          options: {
                            plugins: function () { // post css plugins, can be exported to postcss.config.js
                              return [
                                require('precss'),
                                require('autoprefixer')
                              ];
                            },
                            sourceMap: true
                          }
                    }, {
                        loader: "sass-loader", options: {
                            sourceMap: true
                        }
                    }]
                },
                {
                    test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)|\.svg($|\?)/,
                    use: [
                      {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: '../fonts',
                        },
                      },
                    ]
                },
                {
                    test: /\.(png|jpg|gif)$/,
                    use: [
                      {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: '../images',
                        },
                      },
                    ],
                },
                {
                    test: /\.js$/,
                    exclude: /(node_modules|bower_components)/,
                    use: {
                      loader: 'babel-loader',
                      options: {
                        presets: ['babel-preset-env']
                      }
                    }
                },
            ]
        },
        plugins: [
            new MiniCssExtractPlugin({
              // Options similar to the same options in webpackOptions.output
              // both options are optional
              filename: "../css/[name].css"
            }),
            new CopyWebpackPlugin([
                { from: path.join(__dirname, "assets/src/images"), to: path.join(__dirname, "assets/dist/images") }
            ]),
            new BrowserSyncPlugin( {
                    proxy: webUrl
                },
                {
                    files: [path.join(__dirname, "assets/dist/css/*.css"), path.join(__dirname, "assets/dist/js/*.js"), path.join(__dirname, "assets/dist/images/*.*")]
                }
            )
        ],
        optimization: {
            minimizer: [
              new UglifyJsPlugin({
                cache: true,
                parallel: true,
                sourceMap: true // set to true if you want JS source maps
              }),
              new OptimizeCSSAssetsPlugin({})
            ]
        },
    }
}