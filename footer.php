<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
	<?php do_action('wp_content_bottom'); ?>
	</div>
	<footer id="site-footer">
		<div>
			<div class="row">
				<div class="footer-map col-md-6">
					<?php echo do_shortcode('[lg-map id=42]'); ?>
				</div>
				<div class="footer-contact-info bg-primary col-md-6 p-5">
					<?php  
						$our_hours = get_field("our_hours", "options");
						$footer_logo = get_field("footer_logo", "options"); 
					?>
					<?php if ($footer_logo): ?>
						<div class="mb-5 footer-logo"><img class="d-block w-50 m-auto" src="<?php echo $footer_logo['url']; ?>" alt="$footer_logo['alt']"></div>
					<?php endif; ?>
					<div class="footer-address"><?php do_shortcode("[lg-address]"); ?></div>
					<div class="footer-phone-email text-center">
						<p>
							Call Us : 
							<a href="tel:<?php echo do_shortcode("[lg-phone-main]") ; ?>">
								<?php echo do_shortcode("[lg-phone-main]") ; ?>
							</a>
						</p>
						<p>
							Email Us : 
							<a href="mailto:<?php echo do_shortcode("[lg-email]"); ?>">
								<?php echo do_shortcode("[lg-email]"); ?>	
							</a>
						</p>
					</div>
					
					<?php if($our_hours) : ?>
						<div class="our-hours"><?php echo $our_hours; ?></div>
					<?php endif; ?>
				</div>
			</div>
			<div class="footer-bottom d-flex justify-content-between py-3 py-sm-4 px-3">
				<?php $bottom_footer_logo = get_field('bottom_footer_logo', 'option') ?>
				<?php if ($bottom_footer_logo): ?>
				<div class="footer-bottom-logo">
					<img src="<?php echo $bottom_footer_logo['url']; ?>" alt="<?php echo $bottom_footer_logo['alt']; ?>">
				</div>
				<?php endif ?>
				<a class="footer-longevity" href="https://www.longevitygraphics.com" target="_blank">Web Design by Longevity Graphics</a>
				<a class="footer-cta px-2 py-1 border border-dark" href="tel:<?php echo do_shortcode('[lg-phone-main]') ?>"><i class="fas fa-phone"></i><?php echo do_shortcode("[lg-phone-main]"); ?></a>
			</div>
		</div>
	</footer><!-- #colophon -->
<?php wp_footer(); ?>
</body>
</html>

<?php do_action('document_end'); ?>
