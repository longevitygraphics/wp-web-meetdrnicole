<?php

	switch ( get_row_layout()) {
		case 'our_services':
			get_template_part('/layouts/layouts/our-services');
		break;
		case 'two_grid_multi_img':
			get_template_part('/layouts/layouts/two-grid-multi-img');
		break;
		case 'contact_section':
			get_template_part('/layouts/layouts/contact-section');
		break;
	}

?>