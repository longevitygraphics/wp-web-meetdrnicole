<?php

	get_template_part('/layouts/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

	<div class="d-flex flexible_text <?php if($container == 'container-wide'){echo 'no-gutters';} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
		<div class="col-12">
			<?php 
				$contact_form = get_sub_field("contact_form");
				$right_image = get_sub_field("right_image");
			 ?>
			 <div class="row">
			 	<div class="col-xl-3"></div>
			 	<?php if ($contact_form): ?>
			 		<div class="col-xl-6"><?php echo $contact_form ?></div>
			 	<?php endif ?>
			 	<?php if ($right_image): ?>
			 		<div class="col-xl-3"><img src="<?php echo $right_image['url']; ?>" alt="<?php echo $right_image['alt']; ?>"></div>
			 	<?php endif ?>
			 </div>
		</div>
	</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 
	get_template_part('/layouts/partials/block-settings-end');
?>