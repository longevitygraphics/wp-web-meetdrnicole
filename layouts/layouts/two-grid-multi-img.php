<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->
<?php 
	$multi_imgs = get_sub_field('multi_img');
	$text_section = get_sub_field('text_section');
 ?>
<div class="two-grid-multi-img row">
	<?php if($multi_imgs) : ?>
	 	<div class="multi-images row col-md-6 order-2 order-md-1">
		<?php foreach ($multi_imgs as $image): ?>
		 	<div class="col-sm-6 single-image"><img src="<?php echo $image['url'];  ?>" alt="<?php echo $image['alt']; ?>" /></div>
		<?php endforeach; ?>
		</div>
	<?php endif; ?>
	<?php if ($text_section): ?>
		<div class="two-grid-multi-img-text col-md-6 order-1 order-md-2"><?php echo $text_section;  ?></div>
	<?php endif ?>
</div>
<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>
