<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->
<?php if(have_rows("services")) : ?>
	<div class="our-services row">
	<?php while(have_rows("services")) : the_row();  ?>
	<?php  
		$service_image = get_sub_field("service_image");
		$service_content = get_sub_field("service_content");
	?>
		<div class="service-provided col-md-6 col-lg-4">
			<div class="service-image">
				<img src="<?php echo $service_image['url']; ?>" alt="<?php echo $service_image['alt']; ?>">
			</div>
			<div class="service-content-wrapper">
				<div class="service-content">
					<?php echo $service_content; ?>
				</div>
			</div>
		</div>
	<?php endwhile; ?>
	</div>
<?php endif; ?>
<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>
